var mongodb = require('mongodb');

db = function(){
	this.conf = require('./config.js').confDb;
	this.collection = null;
	this.server = new mongodb.Server(this.conf.dbAddr, this.conf.dbPort, {});
	this.data = null;
	this.err = null;

	this.setCollection = function(colName){
		this.collection = colName;
		return (this);
	}

	this.findOne = function(request, variables){
		oThis = this;
		new mongodb.Db(this.conf.dbName, this.server, {safe: false}).open(function (error, client){
			if (error) throw error;
			new mongodb.Collection(client, oThis.collection).findOne(request, variables, function(err, objects){
				if(err) { oThis.err = err; }
				else { oThis.data = objects; }
			});
	    });
	    return (this);
	}

	this.execute = function(){
		return (this.data);
	}
};

module.exports = new db();

// mongodb.setCollection('blabla').findOne({request})