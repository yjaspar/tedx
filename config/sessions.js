//you config-session.js file
var connectSession = require('connect-session'),
session = connectSession.session,
header = connectSession.header,
util = require('connect-session/lib/utils'),
MemoryStore = require('connect-session/lib/session/store'),
loaders = [header({
  header: 'X-User-Session' //this is used by default, so you can skip this
})],
options = { store: new MemoryStore };

module.exports.sessionCreate = session(loaders, options);
module.exports.sessionLoad = session(loaders, util.merge(options, {generateOnMissingSID: false}));