/**
** Right's weight:
** - Platform's user: 1
** - Platform's client: 2
** - Platform's administrator: 4 _Daphodile_
**/

var confDb = require('./config.js').confDb,
db = require('./config.js').db;

var map = {
	'POST': {
		'/sessions/[a-z0-9{24}]/[a-z0-9]{24}': 1,
	},
	'GET': {
		'/session': 1,
	},
	'PUT': {
		'/sessions': 1,
		'/user': 1
	},
	'DELETE':{
		'/sessions': 4,
	}
};

function checkRight(req, res, next)
{ //req.route
	var found=false;
	req._client=false;
	for (key in map[req.method])
	{
		if (req.url.match('^'+key.replace(/\//g, '\\/')+'$'))
		{
			found=true;
			if ((req._admin&&req.enabled) || (map[req.method][key]==1 && req.userID))
				return (next());
			else if(map[req.method][key]==2 && req.enabled)
			{
				if (req.param('forkId') && req.userID)
				{
					db.collection('Account').findOne(
					{
						'_id': db.ObjectID(req.param('forkId')),
						'owners':{'$all':[req.userID]}
					}, {safe:true},
					function(err, objects)
					{
						if (err)
							return (res.send('Internal error', 500));
						else
						{
							if (objects){
								req._client=true;
								return (next());
							}else
								return (res.send('Unauthorized', 401));
						}
					});
				}
				// @TODO REMPLACER CE PUTAIN DE IF
				else if (req.userID && req.url == "/accounts")
					return (next());		
			}
			else
				return (res.send('Unauthorized', 401));
		}
	}
	if (!found)
		return (res.send('Not found', 404));
};

function proxy(){
	this.checkRight = checkRight; 
};

module.exports = new proxy();