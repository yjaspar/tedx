var mongo = require('mongoose'),
    path = require('path');

module.exports = initConfig;

var conf = {
    'dbAddr': 'localhost',
    'dbPort': '27017',
    'dbName': 'Tedx',
    'appPort': 1337
};

for(k in process.argv){
    k = parseInt(k);
    switch (process.argv[k]){
        case '-p':
            if (process.argv[k+1] != undefined && process.argv[k+1].match(/[0-9]{1,5}/)) {conf.appPort=process.argv[k+1];}
            break;
        case '-d':
            if (process.argv[k+1] != undefined && process.argv[k+1].match(/.+:[0-9]{1,5}/)) {conf.dbAddr=process.argv[k+1];}
            break;
        case '-n':
            if (process.argv[k+1] != undefined) {conf.dbName=process.argv[k+1];}
            break;
    }
}

function initConfig(app, express)
{
    var MongoStore = new require('connect-mongo')(express);

    app.configure(function(){
        app.set('port', conf.appPort);
        app.use(express.logger('dev'));
        app.use(express.bodyParser());
        app.use(express.methodOverride());
        app.use(express.cookieParser('C33.st0'));

        app.use(function (req, res, next) {
            if (req.session)
                req.userID = req.session.user._id;
            next();
        });

        app.use(express.session({
            secret: 'C33.st0',
            maxAge: (1000*60*60*5),
            clear_interval: 3600,
            store: new MongoStore({
                db: conf.dbName,
                collection: 'Session',
                host: conf.dbAddr,
                port: conf.dbPort
            })
        }));
        app.use(app.router);
    });
};

module.exports.db = mongo.connect(conf.dbAddr+':'+conf.dbPort+'/'+conf.dbName, {w: 1});