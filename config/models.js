var db = require('../config/config.js').db;

module.exports.activity = db.model('Activity', db.Schema({
    'client': String,
    'route': {
        'requestedPath': String,
        'method': String,
        'params': String,
        'body': String
    },
    'date': Date,
    'path': String,
    'ip': String,
    'protocol': String,
    'query': String,
    'infoIP': Array,
    'response': {
        'data': String,
        'code': Number
    }
})
);

module.exports.live = db.model('Live', db.Schema({
    title: String,
    author: {
        firstname: String,
        lastname: String,
        biography: String,
        work: String,
        storify: String,
        thumb: String
    },
    metadata: {
        like: Number,
        unlike: Number,
        current: Boolean
    },
    questions: Array,
    replayUrl: String,
    liveUrl: String
})
);

module.exports.user = db.model('User', db.Schema({
    email: String,
    lastLogin: Date,
    password: String,
    salt: String
})
);


module.exports.tweet = db.model('Tweet', db.Schema({
    list: String,
})
);