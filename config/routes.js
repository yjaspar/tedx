module.exports = initRoutes;

function checkAuth(req, res, next){
	if (req.session.user)
		next();
	else
		res.redirect('admin');
}


function initRoutes(app) {
    proxy = require('./proxy.js');
    var ctrl = require('../controllers/index.controller.js');

    // Live route
    app.get('/conferences', new ctrl.live().list);
    app.get('/conferences/:liveID', new ctrl.live().get);
    app.get('/conferences/:liveID/like', new ctrl.live().like);
    app.get('/conferences/:liveID/unlike', new ctrl.live().unlike);
    app.post('/conferences/:liveID/query', new ctrl.live().query);
    app.get('/live', new ctrl.live().live);
    app.put('/conferences/:liveID', new ctrl.live().update);
	
	
	//BO login route
	app.get('/admin', new ctrl.session().show);
	app.post('/admin/login', new ctrl.session().create);
	app.get('/admin/logout', new ctrl.session().delete);
	
	//BO conferences route
	app.get('/admin/conferences',[checkAuth], new ctrl.adminConference().list);
	app.get('/admin/conferences/edit',[checkAuth], new ctrl.adminConference().edit);
	app.get('/admin/conferences/:liveID/edit',[checkAuth], new ctrl.adminConference().edit);
	app.post('/admin/conferences/save',[checkAuth], new ctrl.adminConference().save);
	app.get('/admin/conferences/:liveID/changestatus/:status',[checkAuth], new ctrl.adminConference().changeStatus);
	app.get('/admin/conferences/:liveID/delete',[checkAuth], new ctrl.adminConference().delete);
	app.get('/admin/conferences/:liveID/queries',[checkAuth], new ctrl.adminConference().queries);

	app.get('/admin/users',[checkAuth], new ctrl.adminConference().listUsers);
	app.get('/admin/users/:userID/edit',[checkAuth], new ctrl.adminConference().getUser);
	app.get('/admin/users/new',[checkAuth], new ctrl.adminConference().getUser);
	app.get('/admin/users/:userID/delete',[checkAuth], new ctrl.adminConference().deleteUser);
	app.post('/admin/users/save',[checkAuth], new ctrl.adminConference().saveUser);
	
	//ROUTE TWITTER
	app.get('/tweets', new ctrl.twitter().get);

    //ROUTE
    app.get('/', function(req,res){
          res.redirect('/admin');
    });

    // Default routes
    //app.all('*', function(req, res){ res.json(404, {'responseText': 'Not found'})});
};