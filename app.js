var express = require('express'),
config = require('./config/config.js'),
http = require('http'),
path = require('path'),
express = require('express'),
app = module.exports = express();

require('./config/config.js')(app, express);

app.configure(function(){
	app.engine('ejs', require('ejs-locals'));    
	app.set('views', path.join(__dirname ,'views'));
	app.set('view engine', 'ejs');
	app.use(express.static(path.join(__dirname, 'public')));
});

require('./config/routes.js')(app);


app.listen(app.get('port'), function(){
	console.log("Tedx API runs on port: " + app.get('port'));

    //get Twitter with TedX hashtag
    setInterval(function(){
        require ('./config/twitter.js')();
    },120000);
});

