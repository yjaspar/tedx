var nav = $('.nav');
if (nav) {
    var current = document.location.pathname;
    if(current.indexOf("/admin/users") != -1) {
        nav.find('#users').addClass('active');
    } else {
        nav.find('#conferences').addClass('active');
    }
}