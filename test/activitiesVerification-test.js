var vows   = require('vows'),
db = require('../config/config.js').db,
needle = require('needle'),
assert = require('assert');

vows.describe("Activities verification").addBatch({
	"Verification of the activities from now": {
		topic: function () {
			activity.count({}, this.callback());
		},
		'should have activities in database': function (err, res) {
			assert.isNumber(res);
		}
	}
})
.run({reporter: require('vows/lib/vows/reporters/spec')}, function(sum){
	results.Succeed += sum['honored'];
	results.Failed += sum['broken'];
	results.Errored += sum['errored'];
	console.log(results);
});