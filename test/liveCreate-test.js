var vows   = require('vows'),
db = require('../config/config.js').db,
needle = require('needle'),
assert = require('assert');

live_1 = null;
live_2 = null;
live_3 = null;

vows.describe("Live creation to test the API")
.addBatch({
  "Live#1 creation": {
    topic: function () {

        var live1 = new live({
            title: 'Tedx - 1',
            author: {
                firstname: 'John',
                lastname: 'Doe',
                storify: 'http://john.doe.com',
                thumb: 'http://thumb.com/johndoe'
            },
            metadata: {
                like: 0,
                unlike: 0,
                current: false
            },
            video: false
        });

        live1.save(this.callback);
    },
    'should create a Live in Database': function (err) {
        live_1 =  err;
        assert.isTrue(true);
    }
  }
})
.addBatch({
  "Live#2 creation": {
    topic: function () {

        var live2 = new live({
            title: 'Tedx - 2',
            author: {
                firstname: 'Foo',
                lastname: 'Bar',
                storify: 'http://foo.bar.com',
                thumb: 'http://thumb.com/foobar'
            },
            metadata: {
                like: 0,
                unlike: 0,
                current: false
            },
            video: false
        });

        live2.save(this.callback);
    },
    'should create a Live in Database': function (err) {
        live_2 = err;
        assert.isTrue(true);
    }
  }
})
.addBatch({
  "Live#3 creation": {
    topic: function () {

        var live3 = new live({
            title: 'Tedx - 3',
            author: {
                firstname: 'Hector',
                lastname: 'Nuggets',
                storify: 'http://hector.nuggets.com',
                thumb: 'http://thumb.com/hectornuggets'
            },
            metadata: {
                like: 0,
                unlike: 0,
                current: true
            },
            video: false
        });

        live3.save(this.callback);
    },
    'should create a Live in Database': function (err) {
        live_3 = err;
        assert.isTrue(true);
    }
  }
})
.run({reporter: require('vows/lib/vows/reporters/spec')}, function(sum){
  results.Succeed += sum['honored'];
  results.Failed += sum['broken'];
  results.Errored += sum['errored'];
  require('./live-test.js');
});