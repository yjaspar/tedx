var vows   = require('vows'),
db = require('../config/config.js').db,
needle = require('needle'),
assert = require('assert');

vows.describe("Live creation to test the API")
.addBatch({
  "Unexisting live retrieve": {
    topic: function () {
        needle.get('http://localhost:1337/conferences/519b6c2b594ed1f120001235', {}, this.callback);
    },
    'should get a 404': function (err, res, body) {
        assert.equal(res.statusCode, 404);
    }
  }
})
.addBatch({
  "Live#1 retrieve": {
    topic: function () {
        needle.get('http://localhost:1337/conferences/'+live_1._id, {}, this.callback);
    },
    'should get a 200 and retrive Live#1': function (err, res, body) {
        assert.equal(res.statusCode, 200);
        assert.isObject (body);
    }
  }
})
.addBatch({
  "Live#2 retrieve": {
    topic: function () {
        needle.get('http://localhost:1337/conferences/'+live_2._id, {}, this.callback);
    },
    'should get a 200 and retrive Live#2': function (err, res, body) {
        assert.equal(res.statusCode, 200);
        assert.isObject (body);
    }
  }
})
.addBatch({
  "Retrieve all the live conferences": {
    topic: function () {
        needle.get('http://localhost:1337/conferences', {}, this.callback);
    },
    'should get a 200 and retrive all the lives': function (err, res, body) {
        assert.equal(res.statusCode, 200);
        assert.isArray (body);
    }
  }
})
.addBatch({
  "Retrieve the current live conference": {
    topic: function () {
        needle.get('http://localhost:1337/live', {}, this.callback);
    },
    'should get a 200 and retrive all the lives': function (err, res, body) {
        assert.equal(res.statusCode, 200);
        assert.equal(body._id, live_3._id);
    }
  }
})
.addBatch({
  "Like a fake conference": {
    topic: function () {
        needle.post('http://localhost:1337/conferences/519b6c2b594ed1f120001235/like', {}, this.callback);
    },
    'should get a 404': function (err, res, body) {
        assert.equal(res.statusCode, 404);
    }
  }
})
.addBatch({
  "Like Live#2": {
    topic: function () {
        needle.post('http://localhost:1337/conferences/'+live_2._id+'/like', {}, this.callback);
    },
    'should get a 200': function (err, res, body) {
        assert.equal(res.statusCode, 200);
    }
  }
})
.addBatch({
  "Check the result of the like action on Live#2": {
    topic: function () {
        needle.get('http://localhost:1337/conferences/'+live_2._id, {}, this.callback);
    },
    'should get a 200 and `metadata.like` will be equal 1': function (err, res, body) {
        assert.equal(res.statusCode, 200);
        assert.equal(body.metadata.like, 1);
    }
  }
})
.addBatch({
  "Unlike a fake conference": {
    topic: function () {
        needle.post('http://localhost:1337/conferences/519b6c2b594ed1f120001235/unlike', {}, this.callback);
    },
    'should get a 404': function (err, res, body) {
        assert.equal(res.statusCode, 404);
    }
  }
})
.addBatch({
  "Unlike Live#3": {
    topic: function () {
        needle.post('http://localhost:1337/conferences/'+live_3._id+'/unlike', {}, this.callback);
    },
    'should get a 200': function (err, res, body) {
        assert.equal(res.statusCode, 200);
    }
  }
})
.addBatch({
  "Check the result of the unlike action on Live#3": {
    topic: function () {
        needle.get('http://localhost:1337/conferences/'+live_3._id, {}, this.callback);
    },
    'should get a 200 and `metadata.unlike` will be equal 1': function (err, res, body) {
        assert.equal(res.statusCode, 200);
        assert.equal(body.metadata.unlike, 1);
    }
  }
})
.addBatch({
  "Switch live to Live#1": {
    topic: function () {
        needle.put('http://localhost:1337/conferences/'+live_1._id, "title=Ted - 1&author.firstname=John&author.lastname=Doe&author.storify=http://john.doe.com&author.thumb=http://thumb.com/johndoe&metadata.current=true&video=false", this.callback);
    },
    'should get a 200 and `metadata.unlike` will be equal 1': function (err, res, body) {
        assert.equal(res.statusCode, 200);
    }
  }
})
.addBatch({
  "Live#1 should be the current live": {
    topic: function () {
        needle.get('http://localhost:1337/live', this.callback);
    },
    'should get a 200 and be Live#1': function (err, res, body) {
        assert.equal(res.statusCode, 200);
        assert.equal(body._id, live_1._id);
    }
  }
})
.addBatch({
  "Push query on live 1": {
    topic: function () {
        needle.post('http://localhost:1337/conferences/'+live_1._id+'/query', "title=Why?&body=Because...!",this.callback);
    },
    'should get a 200 and be Live#1': function (err, res, body) {
        assert.equal(res.statusCode, 200);
    }
  }
})
.addBatch({
  "Push query on live 1": {
    topic: function () {
        needle.post('http://localhost:1337/conferences/'+live_1._id+'/query', "title=Why 2?&body=Because 2...!",this.callback);
    },
    'should get a 200 and be Live#1': function (err, res, body) {
        assert.equal(res.statusCode, 200);
    }
  }
})
.run({reporter: require('vows/lib/vows/reporters/spec')}, function(sum){
  results.Succeed += sum['honored'];
  results.Failed += sum['broken'];
  results.Errored += sum['errored'];
  require('./activitiesVerification-test.js');
});