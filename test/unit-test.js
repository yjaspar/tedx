var vows   = require('vows'),
	db = require('../config/config.js').db,
	assert = require('assert');

userdata = {
	"username": "user",
	"firstName":"user-FN",
	"lastName":"user-LN",
	"email":"user@ceesto.org",
	"password":"1234",
	"birthDate": new Date()
	};

clientdata = {
	"username": "client",
	"firstName":"client-FN",
	"lastName":"client-LN",
	"email":"client@ceesto.org",
	"password":"1234",
	"birthDate": new Date()
	};

superuserdata = {
	"username": "superuser",
	"firstName":"superuser-FN",
	"lastName":"superuser-LN",
	"email":"superuser@ceesto.org",
	"password":"1234",
	"birthDate": new Date()
	};

results = {
	"Succeed" : 0,
	"Failed" : 0,
	"Errored" : 0,
}

activity = require('../config/models.js').activity;
live = require('../config/models.js').live;

vows.describe("Database initialization").addBatch({
	"Lives table": {
		topic: function () {
			db.connection.collections['lives'].drop(this.callback());
		},
		'Should return true when initialized': function (err) {
			assert.isTrue(true);
		}
	},
	"Activities table": {
		topic: function () {
			db.connection.collections['activities'].drop(this.callback());
		},
		'Should return true when initialized': function (err) {
			assert.isTrue(true);
		}
	}
}).run({reporter: require('vows/lib/vows/reporters/spec')}, function(sum){
	results.Succeed += sum['honored'];
	results.Failed += sum['broken'];
	results.Errored += sum['errored'];
	require("./liveCreate-test.js");
});
