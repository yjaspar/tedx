function sessionController(){
	
	this.show = function(req, res){
		var oThis = this;
		res.render('authent', {title:'Liste des conférences'});
	}
	
	this.create = function(req, res){
		if (!req.param('login') || !req.param('password'))
            res.render('authent', {title:'Identification', msg:"Email ou mot de passe manquant"});
        else{
            var user = require('../config/models.js').user;
            user.findOne({'email': req.param('login')}, function(err, objects) {                
                if (err) this.sendMongoError(req, res, err);
                else if(objects){
                    var salt=objects.salt;
                    if (objects.password==md5(salt+md5(req.param('password')))){
                        req.session.user = {_id: objects._id};
                        user.update({'_id': objects._id}, {'$set': {'lastLogin': new Date()}}, function(err, ret){
                        	res.redirect('/admin/conferences');
                        });
                    }else{
						res.render('authent', {title:'Identification', msg:"Echec de l'authentification"});
                    }
                }else{
					res.render('authent', {title:'Identification', msg:"Utilisateur introuvable"});
                }
            });
        }
	}
	
	this.delete = function(req, res){
		req.session.destroy();
		res.redirect('/admin');
	}
	
}
sessionController.prototype = new require('./base.controller.js')();

module.exports = sessionController;