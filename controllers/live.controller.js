function liveController(){
	live = require('../config/models.js').live;

	this.list = function(req, res){
		var oThis = this;
		
		live.find({}, function(err, lives){
			if (err) oThis.sendMongoError (req, res, err);
			else{
				if (!live) oThis.sendOk(req, res, 'No conferences has been found');
			 	else oThis.sendOk(req, res, lives);
			}
		});
	}

	this.get = function(req, res){
		var oThis = this;

		live.findOne({'_id': req.param('liveID')}, function(err, live){
			if (err) oThis.sendMongoError (req, res, err);
			else{
				if (!live) oThis.sendNotFound(req, res, 'Conference not found');
				else oThis.sendOk(req, res, live);
			}
		});
	}

	this.like = function(req, res){
		var oThis = this;

		live.update({'_id': req.param('liveID')}, {'$inc':{'metadata.like': 1}}, function(err, live){
			if (err) oThis.sendMongoError (req, res, err);
			else{
				if (!live) oThis.sendNotFound(req, res, 'Conference not found');
				else oThis.sendOk(req, res, true);
			}
		});	
	}

	this.unlike = function(req, res){
		var oThis = this;

		live.update({'_id': req.param('liveID')}, {'$inc':{'metadata.unlike': 1}}, function(err, live){
			if (err) oThis.sendMongoError (req, res, err);
			else{
				if (!live) oThis.sendNotFound(req, res, 'Conference not found');
				else oThis.sendOk(req, res, true);
			}
		});	
	}

	this.query = function(req, res){
		var oThis = this;
		if (req.param('query')){
			live.update({'_id': req.param('liveID')}, {'$push':{'questions': req.param('query')}}, function(err, live){
				if (err) {
                    throw err;
                    oThis.sendMongoError (req, res, err);
                }
				else{
					if (!live) oThis.sendNotFound(req, res, 'Conference not found');
					else {
                        oThis.sendOk(req, res, true);
                    }
				}
			});	
		}else{
            oThis.sendBadParams (req, res, 'Missing parameters');
		}
	}

	this.live = function(req, res){
		var oThis = this;

		live.findOne({'metadata.current': true}, function(err, live){
			if (err) oThis.sendMongoError (req, res, err);
			else{
				if (!live) oThis.sendOk(req, res);
				else oThis.sendOk(req, res, live);
			}
		});
	}

	this.update = function(req, res){
		var oThis = this;
		if (req.param('title') &&
			req.param('author.firstname') &&
			req.param('author.lastname') &&
			req.param('author.storify') &&
			req.param('author.thumb') &&
			req.param('metadata.current')){
			
			live.update({'metadata.current': true}, {'$set':{'metadata.current':false}}, function(err, up){
				if (err) oThis.sendMongoError (req, res, err);
				else{
					live.update({'_id': req.param('liveID')}, {
						'title': req.param('title'),
						'author.firstname' : req.param('author.firstname'),
						'author.lastname' : req.param('author.lastname'),
						'author.storify' : req.param('author.storify'),
						'author.thumb' : req.param('author.thumb'),
						'metadata.current' : req.param('metadata.current'),
						'replayUrl': req.param('replayUrl'),
                        'liveUrl': req.param('liveUrl')
					}, function(err, live){
						if (err) oThis.sendMongoError (req, res, err);
						else{
							if (!live) oThis.sendNotFound(req, res, 'Conference not found');
							else oThis.sendOk(req, res, true);
						}
					});
				}
			});
		}else{
			oThis.sendBadParams (req, res, 'Missing parameters');
		}
	}
}

// Tout le prototype fille est remplacé par celui de la mère
liveController.prototype = new require('./base.controller.js')();

module.exports = liveController;