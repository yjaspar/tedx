function baseController(){
	this.db = require('../config/config.js').db;
	this.md5 = require('MD5');
	this.activity = require('../config/models.js').activity;
	
	this.spy = function(req, res, responseCode, responseText, errorCatched){
	    var data = new activity({
	        'client': 'Guest',
	        'route': {
	            'requestedPath': req.route.path,
	            'method': req.route.method,
	            'params': req.route.params,
	            'body': req.body
	        },
	        'date': new Date(),
	        'path': req.path,
	        'ip': req.ip,
	        'protocol': req.protocol,
	        'query': req.query,
	        'infoIP': require('geoip-lite').lookup(req.ip),
	        'response': {
	            'data': (errorCatched?errorCatched:responseText),
	            'code': responseCode
	        }
	    });
	    
	    data.save(function(err){
	    	if (typeof responseText == 'object' || typeof responseCode == 'object') res.json(responseText, responseCode);
            else res.send(responseText, responseCode);
	    });
	}

	/* System's errors HTTP codes */
	this.sendMongoError = function(req, res, err){
		//console.log(err);
		switch (err.code){
			case 11000:
				this.spy(req, res, 401, 'Email already used', objectData);
				break;
			default:
				this.spy(req, res, 500, 'Internal server error', objectData);
				break;
		}
	}
	
	/* Successful requests HTTP codes */
	this.sendOk = function(req, res, objectData){ this.spy(req, res, 200, objectData); }
	this.sendCreated = function(req, res, objectData){ this.spy(req, res, 201, objectData); }
	
	/* Client's errors HTTP codes */
	this.sendBadParams = function(req, res, objectData){ this.spy(req, res, 400, objectData); }
	this.sendUnauthorized = function(req, res, objectData){ this.spy(req, res, 401, objectData); }
	this.sendForbidden = function(req, res, objectData){ this.spy(req, res, 403, objectData); }
	this.sendNotFound = function(req, res, objectData){ this.spy(req, res, 404, objectData); }
}

module.exports = baseController;