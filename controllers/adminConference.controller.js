function adminConferenceController() {

    live = require('../config/models.js').live;
    user = require('../config/models.js').user;

    this.list = function (req, res, msgError) {

        var oThis = this;

        live.find({}, function (err, lives) {
            if (err) oThis.sendMongoError(req, res, err);
            else {
                if (!live) oThis.sendOk(req, res, 'No conferences has been found');
                else res.render('conferences', {title: 'Liste des conférences', lives: lives, msg: (msgError ? msgError : null)});
            }
        });
    }

    this.delete = function (req, res) {
        var oThis = this;
        live.remove({'_id': req.param('liveID')}, function (err, live) {
            res.redirect('/admin/conferences');
        });
    }

    this.changeStatus = function (req, res) {
        var oThis = this;

        live.update({'metadata.current': true}, {'$set': {'metadata.current': false}}, function (err, up) {
            if (err) oThis.sendMongoError(req, res, err);
            else {
                if(req.param('status')==="current"){
                    live.update({'_id': req.param('liveID')}, {
                        'metadata.current': true
                    }, function (err, live) {
                        if (err) oThis.sendMongoError(req, res, err);
                           else {
                               if (!live) res.redirect('/admin/conferences', 'Conf&eacute;rence introuvable');
                              else res.redirect('/admin/conferences');
                        }
                    });
                } else res.redirect('/admin/conferences');
            }
        });
    }

    this.edit = function (req, res, errorMsg) {
        var oThis = this;
        live.findOne({'_id': req.param('liveID')}, function (err, live) {
            if (err) oThis.sendMongoError(req, res, err);
            else {
                if (!live) res.render('addconf', {title: 'Ajouter une conférence', live: live, error: (errorMsg ? errorMsg : null)});
                else res.render('edit', {title: 'Editer une conférence', live: live, error: (errorMsg ? errorMsg : null)});
            }
        });
    }

    this.save = function (req, res) {
        var oThis = this;

        if (req.param('title') &&
            req.param('author.firstname') &&
            req.param('author.lastname') &&
            req.param('author.storify') &&
            req.param('author.work') &&
            req.param('author.biography') &&
            req.param('author.thumb')) {

            if (req.param('_id')) {
                live.update({'_id': req.param('_id')}, {
                    'title': req.param('title'),
                    'author.firstname': req.param('author.firstname'),
                    'author.lastname': req.param('author.lastname'),
                    'author.work': req.param('author.work'),
                    'author.storify': req.param('author.storify'),
                    'author.biography': req.param('author.biography'),
                    'author.thumb': req.param('author.thumb'),
                    'replayUrl': req.param('replayUrl'),
                    'liveUrl': req.param('liveUrl')
                }, function (err, live) {
                    if (err || !live) oThis.edit(req, res, "Une erreur interne est survenue");
                    else res.redirect('/admin/conferences');
                });
            } else {
                var obj = new live({
                    title: req.param('title'),
                    author: {
                        firstname: req.param('author.firstname'),
                        lastname: req.param('author.lastname'),
                        work: req.param('author.work'),
                        storify: req.param('author.storify'),
                        biography: req.param('author.biography'),
                        thumb: req.param('author.thumb')
                    },
                    metadata: {
                        like: 0,
                        unlike: 0,
                        current: false
                    },
                    queries: [],
                    replayUrl: req.param('replayUrl'),
                    liveUrl: req.param('liveUrl'),
                });

                obj.save(function (err, obj) {
                    if (err || !live) oThis.edit(req, res, "Une erreur interne est survenue");
                    else res.redirect('/admin/conferences');
                });

            }
        } else {
            if (req.param('_id'))
                res.redirect('/admin/conferences/'+req.param('_id')+'/edit');
            else
                res.redirect('/admin/conferences/edit');
        }
    }

    this.queries = function (req, res) {
        var oThis = this;

        live.findOne({'_id': req.param('liveID')}, function (err, live) {
            if (err) oThis.sendMongoError(req, res, err);
            else {
                if (!live) oThis.sendOk(req, res, 'Aucune conférence n\'a été trouvé');
                else res.render('queries', {title: 'Liste des questions', live: live});
            }
        });
    }

    this.listUsers = function (req, res) {
        var oThis = this;

        user.find({}, function (err, users) {
            if (err) oThis.sendMongoError(req, res, err);
            else {
                if (!user) oThis.sendOk(req, res, 'No users has been found');
                else res.render('users', {title: 'Liste des utilisateurs', users: users});
            }
        });
    }

    this.getUser = function (req, res, errorMsg) {
        var oThis = this;

        if (req.param('userID')) {
            user.findOne({'_id': req.param('userID')}, function (err, user) {
                if (err) oThis.sendMongoError(req, res, err);
                else {
                    if (!user) res.render('adduser', {title: 'Ajouter un utilisateur', error: (errorMsg ? errorMsg : null)});
                    else res.render('edituser', {title: 'Editer un utilisateur', user: user, error: (errorMsg ? errorMsg : null)});
                }
            });
        } else {
            res.render('adduser', {title: 'Ajouter un utilisateur', error: (errorMsg ? errorMsg : null)});
        }
    }

    this.deleteUser = function (req, res) {
        user.remove({'_id': req.param('userID')}, function (err, usr) {
            if (err) {
            }
            else
                res.redirect('/admin/users');
        })
    }

    this.saveUser = function (req, res) {
        var oThis = this;

        console.log(req.body);
        if (req.param('email') &&
            req.param('password')) {

            if (req.param('_id')) {
                var usr = {'email': req.param('email')}
                if (req.param('password') != '')
                    usr.password = this.md5(req.param('salt') + this.md5(req.param('password')))
                console.log(usr);
                user.update({'_id': req.param('_id')}, usr, function (err, user) {
                    if (err || !user) oThis.edit(req, res, "Une erreur interne est survenue");
                    else res.redirect('/admin/users');
                });
            } else {
                var salt = this.md5(req.param('email') + (new Date()));

                var usr = new user({
                    email: req.param('email'),
                    password: this.md5(salt + this.md5(req.param('password'))),
                    salt: salt,
                    lastLogin: null
                });

                usr.save(function (err, user) {
                    if (err || !user) oThis.edit(req, res, "Une erreur interne est survenue");
                    res.redirect('/admin/users');
                });
            }
        } else {
            res.redirect('/admin/conferences');
        }
    }
}

adminConferenceController.prototype = new require('./base.controller.js')();

module.exports = adminConferenceController;