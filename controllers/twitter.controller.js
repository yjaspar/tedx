function twitterController() {

    tweet = require('../config/models.js').tweet;

    this.get = function (req, res) {
        var oThis = this;

        tweet.find({}, function (err, t) {
            if (err) oThis.sendMongoError(req, res, err);
            else {
                if (t.length<1) oThis.sendOk(req, res, 'No tweet has been found');
                else {
                    oThis.sendOk(req, res, JSON.parse(t[0].list));
                }
            }
        });
    }

}
twitterController.prototype = new require('./base.controller.js')();

module.exports = twitterController;